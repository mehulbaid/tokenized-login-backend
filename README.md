# Tokenized Login Backend

Created using Nest.Js. Created By [Mehul Baid](https://github.com/mehulbaid).

## Installation

```bash
$ npm i -g yarn
$ npm i -g @nestjs/cli
$ yarn install
```

## Running the app

```bash
$ yarn run start
```


## Stay in touch

- Author - [Mehul Baid](https://www.mehulbaid.tech)

## License

This code is [MIT licensed](LICENSE).
