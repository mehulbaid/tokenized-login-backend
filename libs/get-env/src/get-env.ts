export const getOsEnv = (key: string): string => {
  if (typeof process.env[key] === 'undefined') {
    throw new Error(`Env Variable ${key} is not set.`);
  }

  return process.env[key] as string;
}