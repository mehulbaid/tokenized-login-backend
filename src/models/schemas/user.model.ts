import { Schema, Document } from 'mongoose';
// import * as bcrypt from 'bcrypt';

export interface IUserBasicData extends Document {
  _id: string;
  userToken: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  city: string;
  filename: string;
  username: string;
  password: string;
  isActive: number;
  createdAt: Date;
  updatedAt: Date;
  deleteFlag: number;
}

export const userBasicSchema = new Schema({
  userToken: { type: String },
  firstName: { type: String },
  lastName: { type: String },
  phoneNumber: { type: String },
  city: { type: String },
  filename: { type: String },
  username: { type: String, unique: true },
  password: { type: String },
  isActive: { type: Number, default: 1 },
  deleteFlag: { type: Number, enum: [0, 1], default: 0 }
}, { versionKey: false, timestamps: true });

// export const userModel = model<IUserBasicData>('users', userBasicSchema);