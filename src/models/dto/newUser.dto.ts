import { IsNotEmpty } from 'class-validator';

export class NewUserDto {

  @IsNotEmpty({
    message: `firstName is a required field`
  })
  public firstName: string;

  @IsNotEmpty({
    message: `lastName is a required field`
  })
  public lastName: string;
  
  @IsNotEmpty({
    message: `phoneNumber is a required field`
  })
  public phoneNumber: string;

  @IsNotEmpty({
    message: `city is a required field`
  })
  public city: string;

  @IsNotEmpty({
    message: `imageUrl is a required field`
  })
  public filename: string;

  @IsNotEmpty({
    message: `username is a required field`
  })
  public username: string;

  @IsNotEmpty({
    message: `password is a required field`
  })
  public password: string;
}