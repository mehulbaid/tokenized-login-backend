import { Controller, Post, Body, Res, UseInterceptors, UploadedFile } from '@nestjs/common';
import { AuthService } from './auth.service';
import { NewUserDto } from '@app/models';
import { Response } from 'express';
import { LoginUserDto } from '@app/models/dto/loginUser.dto';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) { }


  @Post('register')
  async newuser(@Body() newUser: NewUserDto, @Res() res: Response) {
    const user = await this.authService.register(newUser);

    if (user.code) {
      if (user.code === 11000) {
        return res.status(200).send({ status: 'failed', data: { errCode: `MongoError ${user.code}`, message: `Specified ${Object.keys(user.keyValue)} value already exists` } });
      } else {
        return res.status(200).send({ status: 'failed', data: `${user.code} MongoError ` })
      }
    } else {
      return res.status(200).send({ status: 'success', data: user });
    }
  }

  @Post('login')
  async login(@Body() body: LoginUserDto, @Res() res: Response) {
    const isVerified = await this.authService.login(body);
    console.log(isVerified);

    if (isVerified == 'username password combination incorrect') {
      return res.status(200).send({ status: 'failed', data: 'username password combination incorrect' });
    }

    if (isVerified.name === 'JsonWebTokenError') {
      return res.status(200).send({ status: 'failed', data: 'UserToken Not Found' })
    }

    return res.status(200).send({ status: 'success', data: isVerified });
  }
}
