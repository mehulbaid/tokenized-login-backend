import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { NewUserDto, IUserBasicData } from '@app/models';
import { LoginUserDto } from '@app/models/dto/loginUser.dto';

@Injectable()
export class AuthService {
  constructor(
    // private usersService: UsersService,
    private jwtService: JwtService,
    @InjectModel('user') private userModel: Model<IUserBasicData>
  ) { }

  async validateUser(username: string, pass: string): Promise<any> {
    try {
      const user = await this.userModel.findOne({ username: username, password: pass });

      console.log(user);
      if (user) {
        const jwtVerified = this.jwtService.verify(user.userToken);

        console.log(jwtVerified);
        if (user.username === jwtVerified.username && user.password === pass) {
          return user;
        }
      }
      return null;
    } catch (error) {
      return error;
    }
  }

  async login(body: LoginUserDto) {
    const user = await this.validateUser(body.username, body.pass);

    if (user === null) {
      return 'username password combination incorrect';
    } else if (user.name === 'JsonWebTokenError') {
      return user;
    } else {
      const payload = { username: user.username };
      return {
        userDetail: user,
        accessToken: this.jwtService.sign(payload)
      };
    }

  }

  async register(newUserData: NewUserDto): Promise<any> {
    try {
      const newUser = new this.userModel(newUserData)
      const userSave = await newUser.save();
      const payload = { username: userSave.username, id: userSave._id };
      const userToken = this.jwtService.sign(payload);

      const addUserToken = await this.userModel.findOneAndUpdate({ _id: userSave._id }, { userToken: userToken })

      return addUserToken.save();
    } catch (error) {
      return error;
    }
  }

  async findAll(): Promise<IUserBasicData[]> {
    return this.userModel.find().exec();
  }
}
