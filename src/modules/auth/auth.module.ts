import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { MongooseModule } from '@nestjs/mongoose';
import { userBasicSchema } from '@app/models';


@Module({
    imports: [
        PassportModule,
        MongooseModule.forFeature([{ name: 'user', schema: userBasicSchema }]),
        JwtModule.register({
            secret: jwtConstants.secret
        })
    ],
    controllers: [
        AuthController
    ],
    providers: [
        AuthService,
    ],
    exports: [AuthService]
})
export class AuthModule { }
