import { config } from 'dotenv';
import { join } from 'path';
import { getOsEnv } from '@app/get-env';

config(
  {
    path: join(process.cwd(), '.env')
  }
);


export const mongodb = {
  URI: getOsEnv('MONGODB_URI')
};