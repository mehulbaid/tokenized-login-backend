import { AuthModule } from './modules/auth/auth.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { mongodb } from './env';
import { MulterModule } from '@nestjs/platform-express';

@Module({
  imports: [
    AuthModule,
    MongooseModule.forRoot(mongodb.URI),
    MulterModule.register({
      dest: './files'
    })
  ],
  controllers: [AppController],
  providers: [
    AppService
  ],
})
export class AppModule { }
